# [1.4.0](https://gitlab.com/erickYataco/cd-ci-elembio/compare/v1.3.0...v1.4.0) (2021-07-06)


### Features

* **version:** check if posible to reset a version ([7d5d619](https://gitlab.com/erickYataco/cd-ci-elembio/commit/7d5d6197f360f7f0bd39d2e900aca0ea7aa608db))

# [1.3.0](https://gitlab.com/erickYataco/cd-ci-elembio/compare/v1.2.0...v1.3.0) (2021-07-06)


### Features

* **bump-demo:** is a demo commit ([e017d28](https://gitlab.com/erickYataco/cd-ci-elembio/commit/e017d286eb6f3a3c0a5ef7c021cb26553deb99a8))

# [1.2.0](https://gitlab.com/erickYataco/cd-ci-elembio/compare/v1.1.0...v1.2.0) (2021-07-05)


### Bug Fixes

* **merge-bump-commits:** just adding a fix to test ([0b5813e](https://gitlab.com/erickYataco/cd-ci-elembio/commit/0b5813e1bbeacc84605beda625f074aab79a05e0))


### Features

* **merge-bump-commits:** testing bump merge with more than one commit ([f6bafcd](https://gitlab.com/erickYataco/cd-ci-elembio/commit/f6bafcdbe6ed6df9ff1b5475e1c7759f7146364d))

# [1.1.0](https://gitlab.com/erickYataco/cd-ci-elembio/compare/v1.0.0...v1.1.0) (2021-07-05)


### Features

* **merge-bump:** making a test of bump version when a merge is approved ([3ddd873](https://gitlab.com/erickYataco/cd-ci-elembio/commit/3ddd87326a7faef2523f781306b89d0f009ba8ae))

# 1.0.0 (2021-07-05)


### Bug Fixes

* **bump-version:** correct name of semantic-release configuration file ([6243c4b](https://gitlab.com/erickYataco/cd-ci-elembio/commit/6243c4b70f2a1723f93f3d3a3d7133f83c766b23))
* **bump-version:** forgoted to commit prepare-commit-msg and gitlab job ([5e11db2](https://gitlab.com/erickYataco/cd-ci-elembio/commit/5e11db2a43e9ccbd2e836dac4fbaaf0316f3bd82))
* **bump-version:** make pipeline deployable ([d9302d4](https://gitlab.com/erickYataco/cd-ci-elembio/commit/d9302d439b0d42ce07004fdd17b3e955c43d6c66))
* **bump-version:** make repository private ([c910e5b](https://gitlab.com/erickYataco/cd-ci-elembio/commit/c910e5b026914fda7253439ba509daed24e04491))


### Features

* **bump-version:** adding commitizen to improve dev experience ([b05bf3c](https://gitlab.com/erickYataco/cd-ci-elembio/commit/b05bf3c58e3af3090619c5e8640b3ccbbaabfb74))
